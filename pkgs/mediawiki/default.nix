args @ { stdenv
, fetchurl
, writeText
, makeWrapper
, bash
, coreutils
, diffutils
, imagemagick
, mw_config
, lib
, php ? args.php71
}:

let
  # FIXME : make use of `<nixpkgs/lib/options.nix>`.
  default_config = {
    id = throw "You must specify `id`";
    dbType = "postgresql";
    dbName = "mediawiki";
    dbServer = ""; # use a Unix domain socket
    dbUser = "mediawiki";
    dbPassword = "";
    emergencyContact = "";#serverInfo.serverConfig.adminAddr;
    passwordSender = "";#serverInfo.serverConfig.adminAddr;
    siteName = "MediaWiki";
    logo = "";
    urlPrefix = "/w";
    articleUrlPrefix = "/wiki";
    enableUploads = false;
    uploadDir = throw "You must specify `uploadDir'.";
    defaultSkin = "";
    skins = [];
    extensions = [];
    extraConfig = "";
  };
  config = default_config // mw_config;
in
with lib;
let

  mediawikiConfig = writeText "LocalSettings.php"
    ''
      <?php
        # Copied verbatim from the default (generated) LocalSettings.php.
        if( defined( 'MW_INSTALL_PATH' ) ) {
                $IP = MW_INSTALL_PATH;
        } else {
                $IP = dirname( __FILE__ );
        }

        $path = array( $IP, "$IP/includes", "$IP/languages" );
        set_include_path( implode( PATH_SEPARATOR, $path ) . PATH_SEPARATOR . get_include_path() );

        require_once( "$IP/includes/DefaultSettings.php" );

        if ( $wgCommandLineMode ) {
                if ( isset( $_SERVER ) && array_key_exists( 'REQUEST_METHOD', $_SERVER ) ) {
                        die( "This script must be run from the command line\n" );
                }
        }

        $wgScriptPath = "${config.urlPrefix}";

        # We probably need to set $wgSecretKey and $wgCacheEpoch.

        # Paths to external programs.
        $wgDiff3 = "${diffutils}/bin/diff3";
        $wgDiff = "${diffutils}/bin/diff";
        $wgImageMagickConvertCommand = "${imagemagick.out}/bin/convert";

        #$wgDebugLogFile = "/tmp/mediawiki_debug_log.txt";

        # Database configuration.
        $wgDBtype = "${config.dbType}";
        $wgDBserver = "${config.dbServer}";
        $wgDBuser = "${config.dbUser}";
        $wgDBpassword = "${config.dbPassword}";
        $wgDBname = "${config.dbName}";

        # E-mail.
        $wgEmergencyContact = "${config.emergencyContact}";
        $wgPasswordSender = "${config.passwordSender}";

        $wgSitename = "${config.siteName}";

        ${optionalString (config.logo != "") ''
          $wgLogo = "${config.logo}";
        ''}

        ${optionalString (config.articleUrlPrefix != "") ''
          $wgArticlePath = "${config.articleUrlPrefix}/$1";
        ''}

        ${optionalString config.enableUploads ''
          $wgEnableUploads = true;
          $wgUploadDirectory = "${config.uploadDir}";
        ''}

        ${optionalString (config.defaultSkin != "") ''
          $wgDefaultSkin = "${config.defaultSkin}";
        ''}

        ${config.extraConfig}
      ?>
    '';
in
stdenv.mkDerivation rec {
  inherit (config) skins extensions;

  _minor = "1.29";
  _patch = "1";
  version = "${_minor}.${_patch}";
  name= "mediawiki-${version}";
  src = fetchurl {
    url = "http://download.wikimedia.org/mediawiki/${_minor}/${name}.tar.gz";
    sha256 = "03mpazbxvb011s2nmlw5p6dc43yjgl5yrsilmj1imyykm57bwb3m";
  };
  buildPhase = ''
    for skin in $skins; do
      cp -prvd $skin/* skins/
    done
    for extension in $extensions; do
      cp -prvd $extension/* extensions/
    done
  '';

  outputs = ["out" "scripts"];
  buildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out
    cp -r * $out
    cp ${mediawikiConfig} $out/LocalSettings.php
    sed -i \
    -e 's|/bin/bash|${bash}/bin/bash|g' \
    -e 's|/usr/bin/timeout|${coreutils}/bin/timeout|g' \
    $out/includes/limit.sh \
    $out/includes/GlobalFunctions.php


    mkdir -p $scripts/bin
    for i in changePassword.php createAndPromote.php userOptions.php edit.php nukePage.php update.php; do
      makeWrapper ${php}/bin/php $scripts/bin/mediawiki-${config.id}-$(basename $i .php) \
        --add-flags $out/maintenance/$i
    done
  '';

  passthru = {
    inherit config;
  };
}
