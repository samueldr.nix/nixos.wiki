self: super:

let
  pkgs = self;
  mkDerivation = self.stdenv.mkDerivation;
  callPackage = self.callPackage;
in
{
  mediawiki = {
    mkMediawiki = (
    mw_config:
    let
      mediawiki = callPackage ./pkgs/mediawiki { inherit mw_config; };
      config = mediawiki.config;
    in
    {
      inherit mediawiki;
      databaseScript = pkgs.writeScript "wiki-${config.id}-database.sh"
      # Initialise the database automagically if we're using a Postgres
      # server on localhost.
      # FIXME : script should:
      #
      # Be split into
      #
      #  * initial-creation (sudo as root)
      #    * db + user creation as postgres
      #    * installation as mediawiki user
      #  * update (expected to be ran as sudo -u mediawiki user)
      #
      (pkgs.lib.optionalString (config.dbType == "postgres" && config.dbServer == "") ''
        #if ! ${pkgs.postgresql}/bin/psql -l | grep -q ' ${config.dbName} ' ; then
        ${pkgs.postgresql}/bin/createuser --no-superuser --no-createdb --no-createrole "${config.dbUser}" || true
        ${pkgs.postgresql}/bin/createdb "${config.dbName}" -O "${config.dbUser}" || :
        ( echo 'CREATE LANGUAGE plpgsql;'
          cat ${mediawiki}/maintenance/postgres/tables.sql
          echo 'CREATE TEXT SEARCH CONFIGURATION public.default ( COPY = pg_catalog.english );'
          echo COMMIT
        ) | ${pkgs.postgresql}/bin/psql -U "${config.dbUser}" "${config.dbName}" || :
        #fi
        ${pkgs.php}/bin/php ${mediawiki}/maintenance/update.php
      '');
    });

    # Theme
    tweeki = mkDerivation rec {
      name = "mediawiki-tweeki";
      src = pkgs.fetchFromGitHub {
        owner = "thaider";
        repo = "tweeki";
        rev = "86645842f4abbdc066fabf451021cd0cf3ff5bf2";
        sha256 = "0hvark5jyzhkzv7hhg8dykg07mp2mjqlhfd35v3bh45r8wvsn125";
      };
      installPhase = "mkdir -p $out/Tweeki; cp -R * $out/Tweeki";
    };
  
    autositemap = mkDerivation rec {
      name = "mediawiki-AutoSitemap";
      src = pkgs.fetchFromGitHub {
        owner = "dolfinus";
        repo = "AutoSitemap";
        rev = "30ed94a47f7fb49ad8442545845a471ea778b066";
        sha256 = "0gama6kq0jbxjgiivy49mlv0k670v55qpprb33fc7kz02rwpis50";
      };
      installPhase = "mkdir -p $out/AutoSitemap; cp -R * $out/AutoSitemap";
    };
  
    usermerge = mkDerivation rec {
      name = "mediawiki-UserMerge";
      src = pkgs.fetchFromGitHub {
        owner = "wikimedia";
        repo = "mediawiki-extensions-UserMerge";
        rev = "REL1_29";
        sha256 = "0x3y12lfs0rr628bmrnqd9w7b1hbzw9qbmkwnpxhxqva9zc092k8";
      };
      installPhase = "mkdir -p $out/UserMerge; cp -R * $out/UserMerge";
    };
  
    # Packages our CSS tweaks.
    nix-theme = mkDerivation rec {
      name = "mediawiki-nix-skin";
      src = ./assets/nix;
      installPhase = "mkdir -p $out/nix; cp -R * $out/nix";
    };
  };
}
