{mediawiki, python35Packages}:

mediawiki.mkMediawiki {
  siteName = "NixOS Wiki";
  id = "nixos.wiki";
  skins = [
    mediawiki.tweeki
    mediawiki.nix-theme
  ];
  extensions = [
    mediawiki.autositemap
    mediawiki.usermerge
    #mediawiki.oauth2client
  ];
  defaultSkin = "Tweeki";
  dbType = "postgres";
  urlPrefix = "";

  extraConfig = builtins.readFile ./config.php + ''
    $wgPygmentizePath = "${python35Packages.pygments}/bin/pygmentize";
  ''
    + ''
        ini_set("display_errors", 1);
        ini_set("display_startup_errors", 1);
        error_reporting(E_ALL);
      ''
    + "/* Development stuff */ $wgShowExceptionDetails = true;"
  ;

  enableUploads = true;
  # Directory will need to be manually managed, and chowned.
  uploadDir = "/srv/local.wiki/uploads";
}
